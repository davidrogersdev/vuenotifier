export default interface StickyMessage {
    text: string;
    type: string;
}