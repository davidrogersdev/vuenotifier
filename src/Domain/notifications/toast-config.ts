export default class ToastConfig {

    constructor(public duration: number = 800, public message: string = "", public type: string = "info", private toastWidth: string = "250px", public position: ToastPosition = new ToastPosition()) {
    }

    get width(): string {
        // use property so may be we do something more sophisticated if need be.
        //console.log(this.toastWidth);
        //console.log(this.message);

        return this.toastWidth;
    }

    set width(toastWidth: string) {
        // use property so may be we do something more sophisticated if need be.        
        this.toastWidth = toastWidth;
    }
}

export class ToastPosition {
    constructor(public my: string = "bottom right", public at: string = "bottom right", public of: string = "window", public offset: string = "0 0") {

    }
}
