import axios, { AxiosInstance } from 'axios'
import notificationService from "@/services/notification-service";

export class AjaxConfig {

    private apiClient: AxiosInstance;

    constructor() {
        this.apiClient = axios.create({
            baseURL: "https://my-json-server.typicode.com/",
            withCredentials: false,
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            }
        });

        this.apiClient.interceptors.response.use(res => {

            if (res.status === 200) {
                // could check message property
                // e.g. if(res.data.outcome.message) 
                //            notificationService.showSuccessMessage(res.data.outcome.message);
                notificationService.showSuccessMessage("This operation Succeeded");
            } else {
                notificationService.showErrorMessage("This operation failed.");
            }

            return res;
        });
    }

    get AppClient(): AxiosInstance {
        return this.apiClient;
    }
}

const ajaxConfig = new AjaxConfig();

export default ajaxConfig;