export default interface ApiResponse<T> {
  data: T;
  outcome: OperationOutcome;
}

export interface OperationOutcome {
  opResult: OpResult;
  errorId: string;
  errors: Array<string>;
  failType: FailType;
  message: string;
}

export enum OpResult {
  Fail = "Fail",
  Success = "Success",
}

export enum FailType {
  None = "None",
  Error = "Error",
  ValidationFailure = "ValidationFailure",
}
