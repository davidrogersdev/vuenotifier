export default class Message {
    constructor(public body: string, public type: string = "success") {

    }
}