import { ActionTree } from "vuex";
import RootState from "../root.state";
import Message from "./message";
import { SHOW_OP_FAIL, SHOW_OP_SUCCESS } from "./mutation.types";
import NotificationState from "./notification.state";

export const actions: ActionTree<NotificationState, RootState> = {
    opFail({ commit }, payload: Message): void {
        commit(SHOW_OP_FAIL, payload);
    },
    opSuccess({ commit }, payload: Message): void {
        commit(SHOW_OP_SUCCESS, payload);
    }
}