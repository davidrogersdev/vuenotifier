import { MutationTree } from "vuex";
import Message from "./message";
import { SHOW_OP_FAIL, SHOW_OP_SUCCESS } from "./mutation.types";
import NotificationState from "./notification.state";

export const mutations: MutationTree<NotificationState> = {
    [SHOW_OP_FAIL](state, payload: Message) {
        state.notifcationMessage = payload;
    },
    [SHOW_OP_SUCCESS](state, payload: Message) {
        state.notifcationMessage = payload;
    }
}