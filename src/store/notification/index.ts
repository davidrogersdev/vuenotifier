import NotificationState from "./notification.state";
import { Namespaced } from "../common";
import { Module } from "vuex";
import RootState from "../root.state";
import { actions } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";
import Message from "./message";

const messageForState = new Message("", "");

export const state: NotificationState = {
    notifcationMessage: messageForState
}

export const notification: Module<NotificationState, RootState> = {
    namespaced: Namespaced,
    state,
    getters,
    actions,
    mutations
}