import { GetterTree } from "vuex";
import RootState from "../root.state";
import Message from "./message";
import NotificationState from "./notification.state";

export const getters: GetterTree<NotificationState, RootState> = {
    message(state: NotificationState): Message {
        return state.notifcationMessage;
    },
}