import { createStore, StoreOptions } from "vuex";
import { notification } from "./notification/index";
import RootState from "./root.state";

const storeOptions: StoreOptions<RootState> = {
  state: {
    version: "1.0.0",
  },
  modules: {
    notification,
  }
}

const store = createStore(storeOptions);

export default store;