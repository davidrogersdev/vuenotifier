import Message from "@/store/notification/message";
import store from "@/store";

export class NotificationService {

    showErrorMessage(errorMessage: string) {
        store.dispatch("notification/opSuccess", new Message(errorMessage, "error"));
    }

    showSuccessMessage(successMessage: string) {
        store.dispatch("notification/opSuccess", new Message(successMessage));
    }
}

const notificationService = new NotificationService();

export default notificationService;