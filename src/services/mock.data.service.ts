import { AxiosInstance } from 'axios'
import ajaxConfig from "@/Domain/ajax/ajax.config";

export class TelevisionShowService {
    private apiClient: AxiosInstance;

    constructor() {
        this.apiClient = ajaxConfig.AppClient;
    }

    getShow(name: string) {
        return this.apiClient.get("DavidRogersDev/DavesMockData/shows");
    }
}

const televisionShowService = new TelevisionShowService();

export default televisionShowService;